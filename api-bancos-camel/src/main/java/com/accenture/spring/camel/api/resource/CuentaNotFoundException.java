package com.accenture.spring.camel.api.resource;

public class CuentaNotFoundException extends Exception {
	
	public void mostrarMensaje() {
		System.out.println("Cuenta no encontrada");
	}

}
