package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dto.Banco;
import com.accenture.spring.camel.api.dto.BancoDao;

@Component
public class ProcesadorExtraccion implements Processor {

	@Autowired
	private BancoDao bancodao;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		
		//creo un banco que alvergue los pocos datos que vienen (id + extraccion)
		Banco extraccion = exchange.getIn().getBody(Banco.class);
		
		//creo un banco auxiliar y le asigno el banco guardado por id en bdd
		Banco banco1 = bancodao.findById(extraccion.getId()).orElse(null);
		
		//sustraigo la extraccion del saldo actual, no toco id
		banco1.setSaldo(banco1.getSaldo() - extraccion.getSaldo());
		
		//finalmente guardo el banco modificado en bdd.
		bancodao.save(banco1);
		
	}

}
