package com.accenture.spring.camel.api.resource;

import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DoFinallyRouteNode;
import org.apache.camel.json.simple.JsonObject;
import org.apache.camel.model.rest.RestBindingMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dto.Banco;
import com.accenture.spring.camel.api.dto.BancoDao;
import com.accenture.spring.camel.api.processor.BancoProcessor;
import com.accenture.spring.camel.api.processor.ProcesadorBDD;
import com.accenture.spring.camel.api.processor.ProcesadorDeposito;
import com.accenture.spring.camel.api.processor.ProcesadorExtraccion;
import com.accenture.spring.camel.api.processor.procesadorBorrar;
import com.accenture.spring.camel.api.service.BancoService;

@Component
public class ApplicationResource extends RouteBuilder {

	@Autowired
	private BancoService service;
	
	@Autowired
	private BancoDao bancodao;

	@BeanInject
	private BancoProcessor processor;
	
	@BeanInject
	private ProcesadorBDD processorBDD;
	
	@BeanInject
	private ProcesadorDeposito procesadordepo;
	
	@BeanInject
	private ProcesadorExtraccion procesadorextr;
	
	@BeanInject
	private procesadorBorrar procesadorb;
	
	@Override
	public void configure() throws Exception {
		restConfiguration().component("servlet").port(9090).host("localhost").bindingMode(RestBindingMode.json);
		
		final Logger log = LoggerFactory.getLogger(ApplicationResource.class);
		//test get
		rest().get("/holamundo").produces(MediaType.APPLICATION_JSON_VALUE).route()
				.setBody(constant("Test basico de API GET")).endRest();
		//deprecated - NO TOMA DE BDD
		rest().get("/getBanco").produces(MediaType.APPLICATION_JSON_VALUE).route().setBody(() -> service.getBanco())
				.endRest();
		//deprecated - NO IMPACTA BDD
		rest().post("/addBanco").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
				.route().process(processor).endRest();
		
		//impacta banco en SQL
		rest().post("/addBanco2").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
				.route().process(processorBDD).endRest();
		
		//carga bancos de la tabla "banco"
		rest("/getBanco2").get().produces(MediaType.APPLICATION_JSON_VALUE).route()
														.setBody(() -> bancodao.findAll()).endRest()
				.get("/{id}").produces(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).route().
								doTry().
									process(new Processor() {
										
										@Override
										public void process(Exchange exchange) throws Exception {
										//Agregar Try{}Catch(CuentaNotFoundException)
											try {
												JsonObject j = new JsonObject();
												j.put("Banco", bancodao.findById(
														Long.parseLong(exchange.getMessage().getHeader("id").toString())
																).orElse(null));
												
												 
												if (j.get("Banco")==null)
													throw (new CuentaNotFoundException());
												else
													exchange.getOut().setBody(j);
											}catch(CuentaNotFoundException e) {
												e.mostrarMensaje();
//												e.printStackTrace();
											}
											
										}
									}).
//									setBody(() -> bancodao.findById("${header.id}")).
								doCatch(CuentaNotFoundException.class).
									to("direct:cuentanotfound").
								doFinally().endRest();
		
		//deposita cierta cantidad de plata pasada desde frontend, necesita ID en body
		rest().put("/depositar").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
				.route().process(procesadordepo).endRest();
		
		//extrae cierta cantidad de plata pasada desde front, necesita ID en body
		rest().put("/extraer").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
			.route().process(procesadorextr).endRest();
		
		//dado solo un ID en un body de tipo Banco.class, borra la cuenta de la BDD
		rest().delete("/eliminar").consumes(MediaType.APPLICATION_JSON_VALUE).type(Banco.class).outType(Banco.class)
		.route().process(procesadorb).endRest();
		
		from("direct:cuentanotfound").log(LoggingLevel.INFO, log, "Cuenta not found").end();
//		
		/**
		from("{{inbound.endpoint}}").
				transacted().
				log(LoggingLevel.INFO, log, "Received message").
								process(new Processor() { @Override 
								public void process(Exchange exchange) throws Exception {
														// TODO Auto-generated method stub
															log.info("Exchange: {}", exchange);}}).
				loop().
				simple("{{outbound.loop.count}}").
				to("{{outbound.endpoint}}").
				log(LoggingLevel.INFO, log, "Message Sent. Iteration: ${property.CamelLoopIndex}").
		end();
		*/
//ver esto: https://stackoverflow.com/questions/50818500/apache-camel-routing-api-call-to-message-queue
		
		//WIP crear un banco y mandarlo al queue del ActiveMQ
//		rest().post("/addBancoQ").type(Banco.class).to("activemq:queue:mi_queue");
		
		//prototipo aproximado a lo que vamos a necesitar
//		rest().get("/getBancoQ").type(Banco.class).to("activemq:queue:mi_queue")
//			.route().process(processor).endRest();
		
	}

}
