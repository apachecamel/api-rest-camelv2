package com.accenture.spring.camel.api.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.accenture.spring.camel.api.dto.Banco;
import com.accenture.spring.camel.api.dto.BancoDao;

@Component
public class ProcesadorDeposito implements Processor {
	
	@Autowired
	private BancoDao bancodao;

	@Override
	public void process(Exchange exchange) throws Exception {
		
		//creo un banco que alvergue los pocos datos que vienen (id + deposito)
		Banco deposito = exchange.getIn().getBody(Banco.class);
		
		//creo un banco auxiliar y le asigno el banco guardado por id en bdd
		Banco banco1 = bancodao.findById(deposito.getId()).orElse(null);
		
		//hago un merge de las 2 variables, quedandome con el deposito nuevo, no toco id
		banco1.setSaldo(banco1.getSaldo() + deposito.getSaldo());
		
		//finalmente impacto el banco modificado en bdd.
		bancodao.save(banco1);
		
	}

}
